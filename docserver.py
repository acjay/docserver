"""
Breakrs, LLC

Simple app for serving Sphinx docs on Google AppEngine

This app simply provides the redirect to log the user out and handles
redirection of the bare URL to index.html.
"""

import webapp2

from google.appengine.api import users

class Index(webapp2.RequestHandler):
    """
    Handle redirect for index page, since the static_files
    handler won't, by default.
    """
    def get(self):
        self.redirect('/index.html')

class Logout(webapp2.RequestHandler):
    """
    Handle logouts
    """
    def get(self):
        self.redirect(users.create_logout_url('/'))

app = webapp2.WSGIApplication(debug=True)
app.router.add((r'/logout', Logout))
app.router.add((r'/', Index))