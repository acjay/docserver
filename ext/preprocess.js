// Designed to add logout link to a sphinxdoc template

$(document).ready(function () {
    $("h3:contains('Navigation')").next().prepend('<li class="right"> (<a href="/logout"  id="logout-link">logout</a>)</li>');
});