Docserver
=========

This simple application is designed to facillitate private hosting of
static content on [Google AppEngine](https://appengine.google.com/),
using on AppEngine's authentication mechanisms.  The app itself does
very little, the "magic" mostly being the use of configuration to
delegate responsibility to the Google APIs, but the directions below
explain how to deploy it.

Serving Sphinx files
--------------------

Docserver can serve any static content, but it was originally created
to serve Sphinx docs. To do so, define a `DOCSERVER` variable at the
top of your Sphinx makefile, and point it to the folder where your
Docserver sits, e.g.:

    DOCSERVER     = ../docserver

Add the following target to your Sphinx makefile, somewhere after the
`html` target:

    docserver-html: html
    	rm -rf $(DOCSERVER)/webroot
    	mkdir -p $(DOCSERVER)/webroot
    	cp -R $(BUILDDIR)/html/* $(DOCSERVER)/webroot
    	find $(DOCSERVER)/webroot -name *.html -exec sed -i '' 's:</head>:<script src="/ext/preprocess.js"></script>&:' {} \;
    	@echo
    	@echo "Deployment finished. The HTML pages are in $(DOCSERVER)/html."

Then run `make docserver-html` from your Sphinx directory, which will
build your docs in the standard way and then copy them into the
Docserver's `webroot` folder.

### Hooks

This app is designed to serve Sphinx docs as is, but followed strictly,
there would be no logout links. So these, and any other content that
should be inserted, must be injected after the fact.  The makefile rule
above allows this to be accomplished by inserting a reference to a
script called `ext/preprocess.js` into every HTML file, and using this,
arbitrary JS code can hooked in to further modify the HTML files. The
default `preprocess.js` is designed to add a logout link to `sphinxdoc`
themed documentation.  Currently, preprocess will apply to every HTML
file, although it could be possible to change behavior based on
`window.location.pathname`.

Deploying Docserver to AppEngine
--------------------------------

In order to test and deploy Docserver, you must install the
[AppEngine Python SDK](https://developers.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python).

The Docserver may be tested locally by runnning the GoogleAppEngineLauncher,
adding the Docserver folder to the console using File->Add Existing
Application, then pressing the Run button.

For deployment, first create and configure an AppEngine Application to
serve the docs, with the desired settings for authentication. The app
may be deployed from the GoogleAppEngineLauncher by pressing the Deploy
button, but this can only be done if you have administrator priveleges
for the Google Apps account associated with your AppEngine Application.

For authentication through your Google Apps domain, you must add the
AppEngine Application.  Go to `Services` under `Organization & users`,
and use the link under "Click here to add more Google Services to your
domain".  There, you can add the AppEngine ID of your application.

To serve the application in a subdomain, follow the instructions under
the Domain Setup subsection Applications Settings in the AppEngine
screen for you application.

Other info
----------

### Author

**Alan Johnson**

+ http://bitbucket.com/acjohnson55

### Copyright and license

Copyright 2013 [Breakrs, LLC](http://www.breakrs.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.